class Cbox < ActiveRecord::Base

  has_one :installation
  has_one :user, through: :installation

  validates :serial, presence: true,
                     uniqueness: true
  
  validates :qrcode_url, presence: true
  

  before_validation(on: :create) do
    self.qrcode_url = "https://chart.googleapis.com/chart?cht=qr&&chs=128x128&chl=" + self.serial
  end



end