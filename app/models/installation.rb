class Installation < ActiveRecord::Base

  attr_accessor :box_serial
  
  belongs_to :user
  belongs_to :cbox

  validates :user,  presence: true
  validates :box_serial, presence: true, on: :create
  validate :check_serial
  #validates :name,  presence: true

  before_save(on: :create) do
    self.name = self.name || "box_#{user.cboxes.size}"
  end

  def check_serial
    # return box_serial is not present, this case is asserted separately
    return unless box_serial.present?
    c = Cbox.find_by(:serial => box_serial)
    if c.blank?
      errors.add(:serial, "needs to be a valid number")
    elsif c.installation.present? && c.installation.id != self.id
      errors.add(:serial, "already assigned to an installation")
    else
     self.cbox = c
    end
  end

  def box_serial
    if self.cbox
      @box_serial ||= self.cbox.serial
    else
      @box_serial
    end
  end

end
