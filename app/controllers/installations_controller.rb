class InstallationsController < ApplicationController

  def index
    @installations = current_user.installations
  end

  def show
    @installation = Installation.find(params[:id])
  end

  # create a new Cbox
  def new
    @installation = Installation.new
  end

  def create
    @installation = Installation.new({user: current_user, box_serial: installation_params[:box_serial]})

    if @installation.save
      flash[:success] = "Box registered: #{@installation.name}"
      redirect_to current_user
    else
      # TODO flash error
      render 'new'
    end
  end

  def edit
    @installation = Installation.find(params[:id])
  end

  def update
    @installation = Installation.find(params[:id])
    if @installation.update_attributes(installation_params)
      flash[:success] = "Box updated: #{@installation.name}"
      redirect_to current_user
    else
      render 'edit'
    end
  end

  def destroy
  end

  private

  def installation_params
    params.require(:installation).permit(:name, :box_serial)
  end
end
