namespace :cboxes do
  desc 'Builds 10 more cboxes and adds them to table'
    task build_10: :environment do
          puts '============= Building 10 more boxes! =============='
          puts ""
          for i in 0..9
            serial = SecureRandom.urlsafe_base64(8)
            cbox = Cbox.create(:serial => serial)
            if cbox.present? 
               puts "Created Cbox[#{cbox.id}] with serial: #{cbox.serial}"
            else
               puts "Failed to create box with serial: #{cbox.serial}"          
            end
          end
    end 
end