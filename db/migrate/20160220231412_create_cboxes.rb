class CreateCboxes < ActiveRecord::Migration
  def change
    create_table :cboxes do |t|
      t.string :serial, null:false, index: true
      t.timestamps null:false
    end
  end
end
