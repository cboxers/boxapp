class AddQrcodeToCboxes < ActiveRecord::Migration
  def change
    add_column :cboxes, :qrcode_url, :string
  end
end
