class CreateInstallations < ActiveRecord::Migration
  def change
    create_table :installations do |t|
      t.string :name, null: false
      t.integer :user_id, index:true
      t.integer :cbox_id, index: true
      t.timestamps null: false
    end
  end
end
