require 'test_helper'

class InstallationTest < ActiveSupport::TestCase

  def setup
    # Create a user
    @user = users(:one)
    #@user.cboxes << cboxes(:one)
    #@cbox = Cbox.create(serial: "123456")
    @cbox = cboxes(:one)
    @cbox2 = cboxes(:two)
    @inst = Installation.new(user: @user,box_serial: "123456")
  end

  test "should be valid" do
    assert @inst.valid?
  end

  test "serial number of cbox should be present" do
    @inst.box_serial = ""
    assert_not @inst.valid?
  end

  test "should reject cbox serial number that does not exist" do
    @inst.box_serial = "11111"
    assert_not @inst.valid?
  end
  
  test "should reject serial number belonging to another installation" do
    @inst2 = Installation.create(user: @user, box_serial: "654321")    
    @inst.box_serial = "654321" 
    # @inst should reject the serial number
    assert_not @inst.valid?
  end
  
  # this test verifies a box can be un-associated after the installation is created
  # the test passes if validation for box serial number presence is ignored on :update
  test "cbox can be retired from existing installation" do
    @inst.box_serial = ""
    assert @inst.valid?(:update)
  end

  test "should have a default name after save" do
    @inst.save
    assert @user.installations.first.name == "box_0"
  end
  
  
end
