require 'test_helper'

class CboxTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @cbox = Cbox.new(serial: '1234567890')
  end

  test "should be valid" do
    assert @cbox.valid?
  end
  
  test "should belong to at most one installation" do
    assert @cbox.installation.blank? || (@cbox.installation.count = 1)
  end

  test "should have serial numnber" do
    @cbox.serial = ""
    assert_not @cbox.valid?
  end
 
  test "should have qrcode link after validation" do
    @cbox.valid?
    assert @cbox.qrcode_url.present?
  end

end
