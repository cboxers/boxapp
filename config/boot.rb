ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'bundler/setup' # Set up gems listed in the Gemfile.


# HHG says: Rails changed the default behavior for WEBrick around version 4 
# Instead of binding to 0.0.0.0, it will now default to localhost
# This implies remote connections will not hit the WEBrick-served pages
# The following snippet forces Rails back into the old universal address 

require 'rails/commands/server'
# Enforce WEBrick classic port and host
module Rails
  class Server
    def default_options
      super.merge(Host: '0.0.0.0', Port: 4000)
    end
  end
end
