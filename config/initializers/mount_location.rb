
env_file = File.join(File.expand_path('../',__FILE__), 'config', 'local_env.yml')
YAML.load(File.open(env_file)).each do |key, value|
   ENV[key.to_s] = value
  puts ENV
end if File.exists?(env_file)





# Mount this application to a unique subdirectory
# You can either use Rails.application.config or <AppName>::Application.config
Rails.application.config.relative_url_root = ENV['RAILS_RELATIVE_URL_ROOT']
