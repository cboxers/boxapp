require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'yaml'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# HHG says:
# To load environmental variables specific to this local, such as 
# ENV['RAILS_RELATIVE_URL_ROOT'] for production. This should probably be 
# within the Application class. But if  thus placed then Sprockets generates 
# compiled assets without the correct relative url root 
env_file = File.join(File.expand_path('../',__FILE__), 'local_env.yml')
if File.exists?(env_file)
  params = YAML.load(File.open(env_file))
  params[Rails.env].each do |key, value|
    ENV[key.to_s] = value
  end if params[Rails.env].present?
end



module Boxapp
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
  end
end
